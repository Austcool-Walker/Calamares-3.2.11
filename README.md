### Calamares Modules : Distribution-Independent Installer Framework additions from EndeavourOS netinstall implementation to modules

## This fork is: [Modified to install EndeavourOS offline/online](https://github.com/endeavouros-team/calamares_config_next)

[![Maintenance](https://img.shields.io/maintenance/yes/2020.svg)]()
[![GitHub release](https://img.shields.io/github/release/calamares/calamares.svg)](https://github.com/calamares/calamares/releases)
[![GitHub license](https://img.shields.io/github/license/calamares/calamares.svg)](https://github.com/calamares/calamares/blob/master/LICENSE)

See [wiki](https://github.com/calamares/calamares/wiki) for up to date
[building](https://github.com/calamares/calamares/wiki/Develop-Guide)
and [deployment](https://github.com/calamares/calamares/wiki/Deploy-Guide)
instructions.
