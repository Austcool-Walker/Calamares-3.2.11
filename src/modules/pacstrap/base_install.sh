#!/bin/bash

_make_pacstrap_calamares(){

if [ ! -f "/usr/bin/pacstrap_calamares" ]

then

sed -e '/chroot_add_mount proc/d' -e '/chroot_add_mount sys/d' -e '/ignore_error chroot_maybe_add_mount/d' -e '/chroot_add_mount udev/d' -e '/chroot_add_mount devpts/d' -e '/chroot_add_mount shm/d' -e '/chroot_add_mount \/run/d' -e '/chroot_add_mount tmp/d' -e '/efivarfs \"/d' /usr/bin/pacstrap >/usr/bin/pacstrap_calamares
chmod +x /usr/bin/pacstrap_calamares

fi

}
  
# Install base system + endeavouros packages + copy necessary config files

_packages_array=( base sudo grub os-prober endeavouros-keyring endeavouros-mirrorlist grub2-theme-endeavouros xterm mkinitcpio mkinitcpio-busybox mkinitcpio-nfs-utils diffutils inetutils jfsutils less logrotate man-db man-pages mdadm nano netctl perl s-nail sysfsutils systemd-sysvcompat texinfo usbutils vi which linux linux-firmware device-mapper cryptsetup e2fsprogs f2fs-tools btrfs-progs lvm2 reiserfsprogs xfsprogs exfatprogs )

_chroot_path=$(cat /tmp/chrootpath.txt) # can't be stored as string

_pacstrap="/usr/bin/pacstrap_calamares -c"

for pkgs in "${_packages_array[*]}"
do
    $_pacstrap -c $_chroot_path $pkgs
done

# run_once needed to be present for chrooted_cleaner_script to detect online install
touch /tmp/run_once

_files_to_copy=(

/usr/bin/{chrooted_cleaner_script,cleaner_script}.sh
/etc/pacman.conf
/tmp/run_once
/etc/default/grub
/etc/mkinitcpio.conf

)

for copy_files in "${_files_to_copy[@]}"
do
    rsync -vaRI $copy_files $_chroot_path
done

}

############ SCRIPT STARTS HERE ############
_make_pacstrap_calamares
############ SCRIPT ENDS HERE ##############
